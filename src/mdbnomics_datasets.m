function datasets = mdbnomics_datasets(varargin) % --*-- Unitary tests --*--
% function mdbnomics_datasets(varargin)
% Downloads the list of available datasets for a selection of providers (or all of them) from https://db.nomics.world/.
% By default, the function returns a structure with a cell array containing the dataset codes and names of the requested providers.
%
% POSSIBLE PARAMETERS
%   provider_code                [char]        DBnomics code of one or multiple providers. If empty, the providers are firstly 
%                                              dowloaded with the function mdbnomics_providers and then the available datasets are requested.
%   simplify                     [logical]     If true, when the datasets are requested for only one provider then a cell array is returned, not a structure. 
%                                              If not provided, the default value is false. 
%
% OUTPUTS
%   datasets
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global mdb_options

p = inputParser;
validStringInput = @(x) ischar(x) || iscellstr(x);
p.addParameter('provider_code', '', validStringInput);
p.addParameter('simplify', false, @islogical);
p.KeepUnmatched = false;
p.parse(varargin{:});

if isempty(p.Results.provider_code)
    provider_code = mdbnomics_providers('code', true);
else
    if ischar(p.Results.provider_code)
        provider_code = {p.Results.provider_code};
    else
        provider_code = p.Results.provider_code;
    end
end

datasets = struct();
for i = 1:numel(provider_code)
    pc = provider_code{i};
    provider_page = sprintf('%s/v%d/providers/%s', mdb_options.api_base_url, mdb_options.api_version, pc);
    provider_info = webread(provider_page);
    provider_info = provider_info.category_tree;
    code = [];
    name = [];
    if isfield(provider_info, 'children')
        unpack_children(provider_info, code, name);
    else
        try
            for n = 1:numel(provider_info)
                code = [code, {provider_info{n}.code}];
                name = [name, {provider_info{n}.name}];
            end
        catch
            for n = 1:numel(provider_info)
                code = [code, {provider_info(n).code}];
                name = [name, {provider_info(n).name}];
            end
        end
    end
    datasets.(pc) = horzcat(code', name');
end

if p.Results.simplify
    if length(fieldnames(datasets)) == 1
        datasets = datasets.(pc);
    else
        error('Your query corresponds to multiple providers, not possible to simplify');
    end
end
end

%@test:1
%$ try
%$    datasets = mdbnomics_datasets('provider_code', 'IMF', 'simplify', true);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(datasets(:,1))), 43); 
%$    t(3) = dassert(size(datasets, 2), 2); 
%$ end 
%$
%$ T = all(t);
%@eof:1

%@test:2
%$ try
%$    datasets = mdbnomics_datasets('provider_code', 'IMF');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(fieldnames(datasets), {'IMF'});
%$    t(3) = dassert(size(datasets.IMF,1), 43); 
%$ end 
%$
%$ T = all(t);
%@eof:2

%@test:3
%$ try
%$    datasets = mdbnomics_datasets('provider_code', {'IMF', 'AMECO'});
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(fieldnames(datasets), {'IMF'; 'AMECO'});
%$    t(3) = dassert(size(datasets.IMF,1), 43);
%$    t(4) = dassert(size(datasets.AMECO,1), 473); 
%$ end 
%$
%$ T = all(t);
%@eof:3