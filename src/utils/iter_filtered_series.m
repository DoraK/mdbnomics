function filtered_series_list = iter_filtered_series(series_list, dbnomics_filters, apply_endpoint_url)
% function iter_filtered_series(series_list, dbnomics_filters, apply_endpoint_url)
% Adapts series to make POST request. Returns cell array of filtered series.
%
% INPUTS
%   series_list             [cell array]       cell array of series previously requested
%   dbnomics_filters        [string]           string array of filters to apply on series
%   apply_endpoint_url      [string]           modified editor API link
%
% OUTPUTS
%   filtered_series_list
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

editor_apply_endpoint_nb_series_per_post = 100;
opts = weboptions('ContentType','json', 'MediaType','application/json', 'RequestMethod','POST');

if size(series_list, 2) > editor_apply_endpoint_nb_series_per_post
    grouped_series = mat2cell(series_list,1,repmat(editor_apply_endpoint_nb_series_per_post, size(series_list,1), size(series_list,2)));
else
    grouped_series = {series_list};
end

for gg = 1:size(grouped_series, 2)
    series_list = grouped_series{gg};
    posted_series_list = cell(1,size(series_list,2));
    series_fields = {'x_frequency', 'period_start_day', 'value'};
    posted_series_fields = {'frequency', 'period_start_day', 'value'};
    for series = 1:size(series_list, 2)
        if ~iscell(series_list{series}.value)
            series_list{series}.value = num2cell(series_list{series}.value);
        end
        for ii = 1:length(posted_series_fields)
            posted_series.(posted_series_fields{ii}) = series_list{series}.(series_fields{ii});
        end
        posted_series_list{series} = posted_series;
    end

    if matlab_ver_less_than('9.1')
        posted_series_list = savejson('',posted_series_list, 'Compact', 1);
        posted_series_list = regexprep(posted_series_list,{' [[',']]'},{'[',']'});
    else
        posted_series_list = jsonencode(posted_series_list);
    end

    json_request = sprintf('{"filters":%s,"series":%s}', dbnomics_filters, posted_series_list);

    try
        response = webwrite(apply_endpoint_url, json_request, opts);
    catch ME
        error_message = ['Could not fetch data from URL: ' apply_endpoint_url ' because: ' ME.identifier];
        if strcmp(ME.identifier, 'MATLAB:webservices:HTTP400StatusCodeError')
            error_message = sprintf('%s.\nPlease revise your input for the filter.', error_message);
        end
        error(error_message);
    end

    filtered_series_list = cell(1,size(series_list,2));
    for ii = 1:length(response.filter_results)
        filtered_series = flatten_editor_series(response.filter_results(ii).series, series_list{ii});
        filtered_series_list{ii} = filtered_series;
    end
end
end
