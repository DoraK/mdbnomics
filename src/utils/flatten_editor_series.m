function series = flatten_editor_series(series, dbnomics_series)
% function flatten_editor_series(series, dbnomics_series)
% Adapts Time Series Editor series attributes to ease cell array construction.
%
% INPUTS
%   series                  [struct]           struct of the filtered series returned by the POST request
%   dbnomics_series         [struct]           struct of the original series returned by the API request
%
% OUTPUTS
%   series
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

series = normalize_period(series);
series = normalize_value(series);

series.x_frequency = series.frequency;
series = rmfield(series, 'frequency');
orig_fields = {'provider_code', 'dataset_code', 'dataset_name'};
for ii = 1:length(orig_fields)
    series.(orig_fields{ii}) = dbnomics_series.(orig_fields{ii});
end

series.series_code = [dbnomics_series.series_code '_filtered'];
if isfield(dbnomics_series, 'series_name')
    series.series_name = [dbnomics_series.series_name, ' (filtered)'];
end
series.filtered = true;
end
