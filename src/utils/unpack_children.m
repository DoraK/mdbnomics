function unpack_children(structure, code, name)
% function unpack_children(structure, code, name)
% Recursively unpacks nested strcuture with fieldname 'children'.
% Returns arrays of dataset codes and names to the caller workspace.
%
% INPUTS
%   structure      [struct]         structure to be unpacked
%   code           [array]          array of dataset codes
%   name           [array]          array of dataset names
%
% OUTPUTS
%   code, name in caller workspace
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

for i = 1:numel(structure)
    if isfield(structure(i), 'children')
        child = structure(i).children;
        if isstruct(child)
            unpack_children(child, code, name)
            continue;
        end
    else
        code = [code, {structure(i).code}];
        name = [name, {structure(i).name}];
    end
end

assignin('caller', 'code', code);
assignin('caller', 'name', name);
end


