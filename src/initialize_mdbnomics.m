function initialize_mdbnomics()

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

% Get the path to the mdbnomics toolbox.
global mdb_options

mdbnomics_src_root = strrep(which('initialize_mdbnomics'), 'initialize_mdbnomics.m', '');

% Set the subfolders to be added in the path.
p = {'utils'};

% Add missing routines if dynare is not in the path
if ~exist('OCTAVE_VERSION', 'builtin') || ~exist('contains','builtin')
    p{end+1} = 'missing/contains';
end

if ~exist('isoctave','file')
    p{end+1} = 'missing/isoctave';
end

if ~exist('matlab_ver_less_than','file')
    p{end+1} = 'missing/matlab_ver_less_than';
end

if ~exist('octave_ver_less_than','file')
    p{end+1} = 'missing/octave_ver_less_than';
end

if exist('OCTAVE_VERSION', 'builtin') && ~exist('user_has_octave_forge_package','file')
    p{end+1} = 'missing/user_has_octave_forge_package';
end

% Set path
P = cellfun(@(c)[mdbnomics_src_root c], p, 'uni', false);
addpath(P{:});

% Check minimal MATLAB requirements.
if matlab_ver_less_than('8.5')
    error('The minimum MATLAB requirement of this package is R2015a.');
end

% Add jsonlab if MATLAB version < R2016b
if matlab_ver_less_than('9.1')
    addpath([mdbnomics_src_root '/../contrib/jsonlab']);
end

mdb_options.api_base_url = 'https://api.db.nomics.world';
mdb_options.editor_base_url = 'https://editor.nomics.world';
mdb_options.api_version = 22;
mdb_options.editor_version = 1;

assignin('caller', 'mdb_options', mdb_options);
assignin('caller', 'mdbnomics_src_root', mdbnomics_src_root);
