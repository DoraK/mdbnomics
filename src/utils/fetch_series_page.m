function response_json = fetch_series_page(series_endpoint_url, offset)
% function fetch_series_page(series_endpoint_url,offset)
% Adapts series_endpoint_url and makes API request. Returns JSON output
% with series characteristics.
%
% INPUTS
%   series_endpoint_url     [string]           API link of series
%   offset                  [integer]          total number of series
%
% OUTPUTS
%   response_json
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

if contains(series_endpoint_url, '?')
    series_page_url = sprintf('%s%soffset=%i', series_endpoint_url, '&', offset);
else
    series_page_url = sprintf('%s%soffset=%i', series_endpoint_url, '?', offset);
end

options = weboptions('ContentType','json');
try
    response_json = webread(series_page_url, options);
catch ME
    error_message = ['Could not fetch data from URL: ' series_page_url ' because: ' ME.identifier];
    error(error_message); 
end

if isempty(response_json.errors)
    series_page = response_json.series;
    if ~isempty(series_page)
        assert(series_page.offset == offset);
    end
else
    error_ = response_json.errors;
    if ~isempty(error_.dataset_code) && isempty(error_.series_code)
        error('%s for: %s. Please revise your input for dataset code.', error_.message, error_.dataset_code);
    elseif ~isempty(error_.provider_code) && isempty(error_.series_code)
        error('%s for: %s. Please revise your input for series provider.', error_.message, error_.provider_code);
    elseif ~isempty(error_.series_code)
        error('%s for: %s. Please revise your input for series code.', error_.message, error_.series_code);
    end
end
end
