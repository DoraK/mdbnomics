function series = mdbnomics_series(varargin)  % --*-- Unitary tests --*--
% function mdbnomics_series(varargin)
% Downloads the list of series for available datasets of a selection of providers from https://db.nomics.world/.
% We warn the user that this function can be (very) long to execute! 
% We remind that DBnomics requests data from 63 providers to retrieve 21675 datasets for a total of approximately 720 millions series.
% By default, the function returns a structure with a cell array at the end of each branch containing the series codes and names of datasets for DBnomics providers.
%
% POSSIBLE PARAMETERS
%   provider_code                [char]        DBnomics code of one or multiple providers. If empty, the providers are firstly 
%                                              dowloaded with the function mdbnomics_providers and then the available datasets are requested.
%   dataset_code                 [char]        DBnomics code of one or multiple datasets of a provider. If empty, the datasets codes are dowloaded 
%                                              with the function mdbnomics_datasets and then the dimensions are requested.
%   dimensions                   [char]        DBnomics code of one or several dimensions in the specified provider and dataset.
%                                              If provided it must be a string formatted like: '{"country":["ES","FR","IT"],"indicator":["IC.REG.COST.PC.FE.ZS.DRFN"]}'.
%   query                        [char]        A query to filter/select series from a provider's dataset.
%   only_number_of_series        [logical]     If true, only the number of series for the given query will be printed in the command window. 
%                                              If not provided, the default value is false.
%   simplify                     [logical]     If true, when the datasets are requested for only one provider then a cell array is returned, not a structure. 
%                                              If not provided, the default value is false. 
%
% OUTPUTS
%   series
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global mdb_options

p = inputParser;
validStringInput = @(x) ischar(x) || iscellstr(x);
p.addParameter('provider_code', '', validStringInput);
p.addParameter('dataset_code', '', validStringInput);
p.addParameter('dimensions', '', validStringInput);
p.addParameter('query', '', validStringInput);
p.addParameter('only_number_of_series', false, @islogical);
p.addParameter('simplify', false, @islogical);
p.KeepUnmatched = false;
p.parse(varargin{:});

if iscell(p.Results.provider_code) || iscell(p.Results.dataset_code)
    if ~isempty(p.Results.provider_code) && ~isempty(p.Results.dataset_code) && length(p.Results.provider_code) ~= length(p.Results.dataset_code)
        error('Please specify as many provider codes as dataset codes.')
    end
end

if isempty(p.Results.provider_code)
    provider_code = mdbnomics_providers('code', true);
else
    if ischar(p.Results.provider_code)
        provider_code = {p.Results.provider_code};
    else
        provider_code = p.Results.provider_code;
    end
end

if isempty(p.Results.dataset_code)
    dataset_code = mdbnomics_datasets('provider_code', provider_code);
else
    if ischar(p.Results.dataset_code)
        dataset_code = {p.Results.dataset_code};
    else
        dataset_code = p.Results.dataset_code;
    end
end

if ~isempty(p.Results.query)
    if ischar(p.Results.query)
        db_query = {p.Results.query};
    else
        db_query = p.Results.query;
    end
end

if ~isempty(p.Results.dimensions)
    if ischar(p.Results.dimensions)
        dimensions = {p.Results.dimensions};
    else
        dimensions = p.Results.dimensions;
    end
end

series = struct();
for i = 1:numel(provider_code)
    pc = provider_code{i};
    dc = dataset_code{i};
    dataset_page = sprintf('%s/v%d/series/%s/%s', mdb_options.api_base_url, mdb_options.api_version, pc, dc);
    if exist('db_query', 'var')
        dataset_page = sprintf('%s?q=%s', dataset_page, db_query{i});
    end
    
    if exist('dimensions', 'var')
        if contains(dimensions{i}, '\\?')
            spec = '&';
        else
            spec = '?';
        end
        dataset_page = sprintf('%s%sdimensions=%s', dataset_page, spec, dimensions{i});
    end
    
    dataset_info = webread(dataset_page);
    dataset_name = sprintf('%s_%s', pc, dc);
    limit = dataset_info.series.limit;
    num_found = dataset_info.series.num_found;
    
    if p.Results.only_number_of_series
        sprintf('Number of series = %d', num_found)
        return
    else
        sprintf('The dataset %s from provider %s contains %d series.', dc, pc, num_found)
        series_code = [];
        series_name = [];

        if num_found > limit
            sequence = 0:1:floor(num_found/limit);
            
            if contains(dataset_page, 'offset=')
                dataset_page = regexprep(dataset_page, '\\&offset=[0-9]+', '');
                dataset_page = regexprep(dataset_page, '\\?offset=[0-9]+', '');
            end
            
            if contains(dataset_page, '\\?')
                sep = '&';
            else
                sep = '?';
            end
            
            for j = 1:numel(sequence)
                tmp_api_link = sprintf('%s%soffset=%d', dataset_page, sep, sequence(j)*limit);
                dataset_info = webread(tmp_api_link);
                series_info = dataset_info.series.docs;
                for s = 1:numel(series_info)
                    series_code = [series_code, {series_info(s).series_code}];
                    series_name = [series_name, {series_info(s).series_name}];
                end
            end
            series.(dataset_name) = horzcat(series_code', series_name');
        else
            series_info = dataset_info.series.docs;
            for s = 1:numel(series_info)
                series_code = [series_code, {series_info(s).series_code}];
                series_name = [series_name, {series_info(s).series_name}];
            end
            series.(dataset_name) = horzcat(series_code', series_name');
        end
    end
end

if p.Results.simplify
    if length(fieldnames(series)) == 1
        series = series.(dataset_name);
    else
        error('Your query corresponds to multiple datasets, not possible to simplify');
    end
end
end

%@test:1
%$ try
%$    series = mdbnomics_series('provider_code', 'IMF', 'dataset_code', 'WEO', 'simplify', true);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(series(:,1))), 8924); 
%$    t(3) = dassert(size(series, 2), 2); 
%$ end 
%$
%$ T = all(t);
%@eof:1

%@test:2
%$ try
%$    series = mdbnomics_series('provider_code', 'IMF', 'dataset_code', 'WEO');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1)
%$    t(2) = dassert(fieldnames(series), {'IMF_WEO'});
%$    t(3) = dassert(length(unique(series.IMF_WEO(:,1))), 8924); 
%$    t(4) = dassert(size(series.IMF_WEO, 2), 2);
%$ end 
%$
%$ T = all(t);
%@eof:2

%@test:3
%$ try
%$    series = mdbnomics_series('provider_code', 'IMF', 'dataset_code', 'WEO', 'dimensions', '{"weo-subject":["NGDP_RPCH"]}', 'simplify', true);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1)
%$    t(2) = dassert(length(unique(series(:,1))), 194); 
%$    t(3) = dassert(size(series, 2), 2);
%$ end 
%$
%$ T = all(t);
%@eof:3

%@test:4
%$ try
%$    series = mdbnomics_series('provider_code', 'IMF', 'dataset_code', 'WEO', 'query', 'NGDP_RPCH');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1)
%$    t(2) = dassert(length(unique(series.IMF_WEO(:,1))), 194); 
%$    t(3) = dassert(size(series.IMF_WEO, 2), 2);
%$ end
%$
%$ T = all(t);
%@eof:4