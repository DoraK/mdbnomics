function series = flatten_dbnomics_series(series)
% function flatten_dbnomics_series(series)
% Adapts DBnomics series attributes to ease cell array construction.
% Normalizes the period and the value attributes, flattens the dimensions
% and observation attributes.
%
% INPUTS
%   series                  [struct]           struct of series previously requested
%
% OUTPUTS
%   series
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

series = normalize_period(series);
series = normalize_value(series);

% Flatten dimensions.
if isfield(series, 'dimensions')
    dimensions = series.dimensions;
    fields_dim = fieldnames(dimensions);
    series = rmfield(series, {'dimensions', 'indexed_at'});
    for ii = 1:length(fields_dim)
        series.(fields_dim{ii}) = dimensions.(fields_dim{ii});
    end
end

% Flatten observation attributes.
if isfield(series, 'observation_attributes')
    observation_attributes = series.observation_attributes;
    fields_obs = fieldnames(observation_attributes);
    series = rmfield(series, 'observation_attributes');
    for ii = 1:length(fields_obs)
        series.(fields_obs{ii}) = observation_attributes.(fields_obs{ii});
    end
else
    series.observation_attributes = [];
end
end